<?php
/*
Template Name: slideshow
*/
?>

<?php //get_template_part('templates/page', 'header'); ?>
<div class="container">
<div class="row">
<div class="span9">

<!-- Carousel     ================================================== -->
<div class="carousel slide" id="myCarousel">
<div class="carousel-inner">
<div class="item active">

<?php echo get_my_post( 'slideshow1' ); ?>
<div class="container">
<div class="carousel-caption">
<h1>Example headline.</h1>
<p class="leads">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>

</div>
</div>
</div>
<div class="item">

<img alt="" src="media/slide/slide-02.jpg" />
<div class="container">
<div class="carousel-caption">
<h1>Another example headline.</h1>
<p class="leads">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
<a class="btn btn-large btn-primary" href="#">Learn more</a>

</div>
</div>
</div>
<div class="item">

<img alt="" src="media/slide/slide-03.jpg" />
<div class="container">
<div class="carousel-caption">
<h1>One more for good measure.</h1>
<p class="leads">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>

</div>
</div>
</div>
</div>
<a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
<a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>

</div>
<!-- /.carousel -->

</div>
<div class="span3 sidebar"> 	<form class="form-horizontal contactform" method="post" action="cr.php">
	<h2> Contact Me </h2>
  <div class="control-group">
    <label class="control-label" for="inputFullname">Full Name/ Company</label>
    <div class="controls">
      <input type="text" name="inputFullname" id="inputFullname" placeholder="Full Name/ Company">
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputEmail">Email</label>
    <div class="controls">
      <input type="text" name="inputEmail" id="inputEmail" placeholder="Email">
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputTel">Landline/Mobile</label>
    <div class="controls">
      <input type="text" id="inputTel" name="inputTel" placeholder="Landline/Mobile">
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputSubject">Subject</label>
    <div class="controls">
      <input type="text" name="inputSubject" id="inputSubject" placeholder="Subject">
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputMsg">Message</label>
    <div class="controls">
      <textarea rows="3" name="inputMsg" id="inputMsg" placeholder="inputMsg"> </textarea>
    </div>
  </div>
  <div class="control-group">
    <div class="controls">
      <button type="submit" class="btn btn-info">Submit</button>
    </div>
  </div>
</form>
</div>
</div>
</div>
<?php //get_template_part('templates/content', 'page'); ?>