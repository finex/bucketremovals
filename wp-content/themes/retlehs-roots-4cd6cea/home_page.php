<?php
/*
Template Name: Homepage
*/
?>

<?php //get_template_part('templates/page', 'header'); ?>
<div class="container  contentwrapper">
<div class="row-fluid">
<div class="span9">

<!-- Carousel     ================================================== -->
<div class="carousel slide hidden-phone " id="myCarousel">
   
	  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
	 
  </ol>
<div class="carousel-inner">
<div class="item active">

<?php echo get_my_post( 'slideshow1' ); ?>
<div class="container">
<div class="carousel-caption">
<h3>Example headline.</h3>
<p class="leads">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>

</div>
</div>
</div>
<div class="item">

<img alt="" src="media/slide/slide-02.jpg" />
<div class="container">
<div class="carousel-caption">
<h3>Another example headline.</h3>
<p class="leads">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>


</div>
</div>
</div>
<div class="item">

<img alt="" src="media/slide/slide-03.jpg" />
<div class="container">
<div class="carousel-caption">
<h3>One more for good measure.</h3>
<p class="leads">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>

</div>
</div>
</div>
</div>
    
<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
  <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>

</div>
<!-- /.carousel -->

</div>
<div class="span3 redbg small-left-margin height300 hidden-phone" style="width:25% !important;"> 
      <legend>Quick Enquiry</legend>
    <div class="sideform"> 
          <?php get_template_part('templates/home', 'quick_enquiry'); ?>
    </div>
</div>
</div>
</div>
 

<?php get_template_part('templates/home', 'marketing'); ?>
<?php get_template_part('templates/home', 'welcome'); ?>