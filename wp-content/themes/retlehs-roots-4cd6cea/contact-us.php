<?php
/*
Template Name: Contact-Us
*/
?>

<?php
 if(isset($_POST['submitted'])) {
     $name = ucwords($_POST['inputFullname']);
$email = $_POST['inputEmail'];
$tel = $_POST['inputTel'];
$subj = $_POST['inputSubject'];
$message = $_POST['inputMsg'];
$message = stripslashes($message);
    $errors=array();
if (!preg_match("/[a-zA-Z]/", "$name")) 
{  $errors[0]  = "Please enter a name.<br />" ; }
    else {"No error" ; }
    
// Validate Email Address
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
{ $errors[1]  = "Please enter a valid email adddress.<br />" ; }
    else {"No error" ; }
    
// Validate Telephone Number
if (preg_match("/[a-zA-Z]/", "$tel"))
{ $errors[2]  = "Please enter a valid telephone number.<br />" ; }
    else {"No error" ; }
            
// Validate Message
if (!preg_match("/[a-zA-Z0-9]/", "$message")) 
{  $errors[3]  = "Please enter a message.<br />" ; }
    else {"No error" ; }
    
// Spam checks for the message    
if (preg_match("/(www.|http:|href|https:)/i", "$message")) 
{ $errors[4]  = "Please remove web links from the message.<br />" ; }
    else {"No error" ; }
    
if ($errors) {
    
    echo "<div class=\"alert container-center  alert-error\">
  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
  <h4>Warning!</h4> There were validation errors please click your browser back button to go back and correct.
<br /><ul>"; 
foreach($errors as $error){
    echo "<li>".$error."</li>";
}
echo "</ul></div>";
}
//// End of validation

// All data valid???
if (!$errors) {
// Build the email
$to = get_option('admin_email');
$subject = "Email From Your BudgetRemovalsUk.com Site: $name";
$message = " <img src='www.budgetremovalsuk.com/media/logo-300x82.jpg'/><br/><h2 style='color:#7D011E;'> <b>You Have got an Email from Your website. Please read below.</b></h2>
<div style='padding:1px 6px 1px 43px;'>
<b style='color:#aa1111;'>Sender Name</b>: $name  <br/>
<b style='color:#aa1111;'>Form Name</b>: Contact us form <br/>
<b style='color:#aa1111;'>Sender E-mail</b>: $email   <br/>
<b style='color:#aa1111;'>Sender Telephone</b>: $tel  <br/>
<b style='color:#aa1111;'>Subject</b>: $subj <br/>
<b style='color:#aa1111;'>Message</b>: <br/>$message   <br/>   </div>
  <br/>
- - - End of Message - - - ";
$headers = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= "From: $email". "\r\n"; 

// Send the mail using PHPs mail() function
$sent = mail($to, $subject, $message, $headers); } 
 if($sent) 
 {
     echo "<div class=\"alert container-center alert-success\">
     <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
     <strong>Thank you " .$name."</strong><p>We will contact you very soon.</p> </div>"; 
 }

    }
    ?>
    <div class="container container-center ">
<!-- Three columns of text below the carousel -->
    
    <form class="form-horizontal contactform" action="<?php the_permalink(); ?>" id="contactForm" method="post">
    <h2> Contact us </h2>
       <?php get_template_part('templates/content', 'page'); ?>
  <div class="control-group">
    <label class="control-label" for="inputFullname">Full Name</label>
    <div class="controls">
      <input type="text" name="inputFullname" id="inputFullname" placeholder="Full Name/ Company">
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputEmail">Email</label>
    <div class="controls">
      <input type="text" name="inputEmail" id="inputEmail" placeholder="Email">
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputTel">Landline/Mobile</label>
    <div class="controls">
      <input type="text" id="inputTel" name="inputTel" placeholder="Landline/Mobile">
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputSubject">Subject</label>
    <div class="controls">
      <input type="text" name="inputSubject" id="inputSubject" placeholder="Subject">
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputMsg">Message</label>
    <div class="controls">
      <textarea rows="3" name="inputMsg" id="inputMsg" placeholder="inputMsg"> </textarea>
    </div>
  </div>
  <div class="control-group">
    <div class="controls">
      <button type="submit" name="submitted" class="btn btn-warning">Submit</button>
    </div>
  </div>
</form>
    </div> 
