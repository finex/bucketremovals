<footer class="container content-info redbg" role="contentinfo">
  <div class=" contentwrapper">
      <div class="sitemap pull-leftt"> <?php
          if (has_nav_menu('primary_navigation')) :
           // wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav'));
          endif;
        ?>
      <div class="footeraddress pull-right">
    <?php dynamic_sidebar('sidebar-footer'); ?>
          
    <address >
        <strong>Budget Removals and Storage </strong><br>
        0112 303 0197 - 07929 829555
        Follow us on <br>
        &copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>
</address>
      </div></div>
  </div>
</footer>
 <script>
      !function ($) {
        $(function(){
          // carousel demo
          $('#myCarousel').carousel()
        })
      }(window.jQuery)
    </script>
<?php wp_footer(); ?>
