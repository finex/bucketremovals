<header class="banner navbar   navbar-static-top" role="banner">
<div class="container-pageheader hidden-phone">
	<div class="container ">
	<div class="pageheader row ">
	<div class="logo pull-left span6">
  <img src="<?php echo home_url('/'); ?>/media/logo.jpg" />
  </div>
  <div class="headernotice push-right span4 offset2">
   <p><?php echo get_my_post( 'headercontacts' ); ?></p>
  </div>
 		</div>
		</div>
		</div>
    <div class="container ">
  <div class="navbar-inner navbar-inverse navbg">
    
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <a class="brand hidden-desktop" href="<?php echo home_url(); ?>/">
        <?php bloginfo('name'); ?>
      </a>
      <nav class="nav-main nav-collapse collapse" role="navigation">
        <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav'));
          endif;
        ?>
      </nav>
    </div>
  </div>
</header>
