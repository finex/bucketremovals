<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'removals');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'M[Nj6;FQG-^Y5h|?bzeg1=myU5`b#gH}jU_du4tadBK:#P8a_mB[Fxu-GcJZ*-vD');
define('SECURE_AUTH_KEY',  'avJ||D>)rX-Ug_~.}#IM9u+{@.CuysyBa^%5v-72SqUX,%a0cg|I.-T+;_WlEtGD');
define('LOGGED_IN_KEY',    '#)>M5<>wV0TvmbH4@-YAR UfDT%+_~H~GtB>mKI `x0I:G|;?^PW{=O(>6`e5t]?');
define('NONCE_KEY',        'OD+G}I>5GDsniYbK^k?>+3}oB!/J?[!OA]X7Y]g`UK(uA+x#)oeix`Th^%U32IA+');
define('AUTH_SALT',        '>-+$gNDJLl;4u[O-1rx7l^yZxy0CU:P~6b1{3&r=J9HI-^nQrS(R6$@zBTA!0%J&');
define('SECURE_AUTH_SALT', 'c}?(.a$)QOrs*irU{~j -kx~_yI3u*-nRyK@xE{{6a&^nC[-`IE8>X+N4gL8A0=S');
define('LOGGED_IN_SALT',   '-((?uVK;G92+(:f}A$ E@`lx/CoQQLVO_~?u^{z~|CJ9;9$v8wY+??4V|B#H&6%<');
define('NONCE_SALT',       'X%82xJ_(L[!y;7 c`?*kEwulBYyTrwTToR{-wQ[pPblF)i_>E@Br+~7nS-JRN=Y.');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
